<?php

return [
    'database' => [
        'driver' => 'pdo_mysql',
        'user' => 'root',
        'password' => 'onehome123',
        'dbname' => 'books',
    ],

    'twig' => [
        'dir' => __DIR__,
        'cache' => __DIR__ . '/../cache'
    ]
];